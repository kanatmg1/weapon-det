from flask import Flask, redirect, render_template, request, sessions, flash, url_for
from werkzeug.utils import secure_filename
import io
import numpy as np
import cv2
import ModelFunc as mf
import json

app = Flask(__name__)
ALLOWED_EXTENSIONS = {'pdf', 'png', 'jpg', 'jpeg',}
UPLOAD_FOLDER = 'templates/upload_folder'
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER

def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

@app.route('/predict', methods=['GET','POST'])
def upload_picture_mobilenet():
    if request.method == 'POST':
        # check if the post request has the file part
        #if 'file' not in request.files:
        #    flash('No file part')
        #    return redirect(request.url)
        #file = request.files['file']
        # if user does not select file, browser also
        # submit an empty part without filename
        content = request.json
        print(content['file_path'])
        #if file.filename == '':
        #    flash('No image file')
        #    return redirect(request.url)
        if content and content['file_path'] != '':
            # file.save(secure_filename(file.filename))
            # print(file, file.filename)
            # print(file.read())
            # file = open(content['file_path'])
            # f = file.read()
            # np_img = np.fromstring(f, np.uint8)
            # print(np_img, np_img.shape)
            #img = cv2.imdecode(np_img, cv2.IMREAD_COLOR)
            try:
                img = cv2.imread(content['file_path'])
                img = cv2.resize(img, (224,224), interpolation = cv2.INTER_CUBIC)
                # print(img, img.shape)
                #temp_f = file.filename[::-1]
                #temp_f = f"{temp_f[temp_f.find('.')+1:][::-1]}"
                #new_path = f'./static/{temp_f}.jpg'
                #cv2.imwrite(new_path, img)
                filename_path = content['file_path']
                dim = (224,224)
                model = mf.get_mobilenet()
                prob, cat = mf.get_img_prediction_bounding_box(filename_path, dim, model)#new_img, lime_img
                #cv2.imwrite(new_path, new_img)
                #cv2.imwrite(f'/home/developer/FlaskApp/static/{temp_f}Lime.jpg', lime_img)
                print(prob, cat)
                if cat in ['Rifle', 'Handgun']:
                    cat = 'Weapon'
                prob = f'{int(prob)}%'
                #response_list = cat
                return cat
            except:
                return "Not found"
            #return render_template('display_photo.html', bounding_image = f'{temp_f}.jpg', lime_image = f'{temp_f}Lime.jpg',cat = cat, prob = prob)
            # return f'<img src="{{ url_for("static", filename = '{temp_f}')}}">'

    return '''
    Get is not allowed
    '''
if __name__ == '__main__':
    app.run(host='0.0.0.0',threaded = True, port = 5000)
