FROM nvidia/cuda:10.1-cudnn7-devel-ubuntu18.04

SHELL ["/bin/bash", "-c"] 

RUN echo "deb http://developer.download.nvidia.com/compute/machine-learning/repos/ubuntu1804/x86_64 /" > /etc/apt/sources.list.d/nvidia-ml.list

ENV TZ=Asia/Almaty
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

RUN apt-get update && apt-get install -y --no-install-recommends \
    autoconf \
    automake \
    libtool \
    pkg-config \
    ca-certificates \
	sudo \
	build-essential \
    software-properties-common \
	cmake \
	git \
	curl \
	vim \
	wget \
	libjpeg-dev \
	libpng-dev \
	ca-certificates \
    python3 \
    python3-dev \
    python \
    python-dev \
    libprotobuf-dev \
    protobuf-compiler \
    cmake \
    swig \
    libgl1-mesa-glx \
    libsm6 \
    libxext6 \
    libxrender1 && \
    rm -rf /var/lib/apt/lists/*


RUN cd /usr/local/src && \
    wget https://bootstrap.pypa.io/get-pip.py && \
    python3 get-pip.py && \
    pip3 install --upgrade pip && \
    rm -f get-pip.py


RUN pip3 install numpy opencv-contrib-python Flask scikit-image tensorflow Keras lime  

RUN export uid=1000 gid=1000 && \
    mkdir -p /home/developer && \
    echo "developer:x:${uid}:${gid}:Developer:/home/developer:/bin/bash" >> /etc/passwd && \
    echo "developer:x:${uid}:" >> /etc/group && \
    echo "developer ALL=(ALL) NOPASSWD: ALL" > /etc/sudoers.d/developer && \
    chmod 0440 /etc/sudoers.d/developer && \
    chown ${uid}:${gid} -R /home/developer

USER developer
ENV HOME /home/developer

WORKDIR /home/developer
COPY ./FlaskApp /home/developer/
